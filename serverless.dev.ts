import type { AWS } from '@serverless/typescript';

const serviceName = 'seek-demo';
// const bucketName = `${serviceName}-bucket`;

const serverlessConfiguration: AWS = {
  service: 'seek-demo',
  frameworkVersion: '3',
  plugins: ['serverless-esbuild', 'serverless-offline'],
  provider: {
    name: 'aws',
    region: 'ap-southeast-2',
    runtime: 'nodejs16.x',
    stage: "${opt:stage, 'dev'}",
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
    // default for all Lambdas
    // iamRoleStatements: [{
    //   Effect: 'Allow',
    //   Action: [
    //     'dynamodb:Query',
    //     'dynamodb:Scan',
    //     'dynamodb:GetItem',
    //     'dynamodb:PutItem',
    //     'dynamodb:UpdateItem',
    //     'dynamodb:DeleteItem'
    //   ],
    //   Resource: [
    //     { "Fn::GetAtt": ["DynamoDBTable", "Arn"] }
    //   ]
    // }, {
    //   Effect: 'Allow',
    //   Action: [
    //     's3:GetObject',
    //     's3:PutObject',
    //     's3:DeleteObject',
    //     's3:ListObjects'
    //   ],
    //   Resource: [
    //     { "Fn::GetAtt": ["ImagesBucket", "Arn"] },
    //     {
    //       "Fn::Join": [
    //         "/",
    //         [{ "Fn::GetAtt": ["ImagesBucket", "Arn"] }, '*']
    //       ]
    //     }
    //   ]
    // }]
  },
  // import the function via paths
  functions: {
    authorization: {
      handler: 'src/handlers/authorizationHandler.handler',
    },
    graphqlPublic: {
      handler: 'src/handlers/graphqlPublicHandler.handler',
      events: [
        {
          http: {
            method: 'POST',
            path: 'public/graphql',
            // authorizer: {
            //   name: 'authorization'
            // }
          },
        },
      ],
    },
    graphqlCustomer: {
      handler: 'src/handlers/graphqlCustomerHandler.handler',
      events: [
        {
          http: {
            method: 'POST',
            path: 'customer/graphql',
            authorizer: {
              name: 'authorization'
            }
          },
        },
      ],
    }
  },
  package: { individually: true },
  custom: {
    esbuild: {
      bundle: true,
      minify: false,
      sourcemap: true,
      exclude: ['aws-sdk'],
      target: 'node14',
      define: { 'require.resolve': undefined },
      platform: 'node',
      concurrency: 10,
    },
  },
  // resources: {
  //   Resources: {
  //     DynamoDBTable: {
  //       Type: 'AWS::DynamoDB::Table',
  //       Properties: {
  //         TableName: `${"${self:provider.stage}"}-dynamodb-table`,
  //         AttributeDefinitions: [
  //           {
  //             AttributeName: 'partitionKey',
  //             AttributeType: 'S'
  //           },
  //           {
  //             AttributeName: 'sortKey',
  //             AttributeType: 'S'
  //           }
  //         ],
  //         KeySchema: [
  //           {
  //             AttributeName: 'partitionKey',
  //             KeyType: 'HASH'
  //           },
  //           {
  //             AttributeName: 'sortKey',
  //             KeyType: 'RANGE'
  //           }
  //         ],
  //         ProvisionedThroughput: {
  //           ReadCapacityUnits: 1,
  //           WriteCapacityUnits: 1
  //         }
  //       }
  //     },
  //     ImagesBucket: {
  //       Type: 'AWS::S3::Bucket',
  //       Properties: {
  //         BucketName: `${"${self:provider.stage}"}-${bucketName}`
  //       }
  //     }
  //   }
  // }
};

module.exports = serverlessConfiguration;
