import { HttpError } from "handlers/errors/HttpError";
import { PricingRule } from "models/PricingRule";
import { Product } from "models/Product";
import { CustomerRepository } from "repository/CustomerRepository";
import { ProductRepository } from "repository/ProductRepository";
import { injectable, inject } from "tsyringe";
import { PricingRulesStrategyFactory } from "services/pricingRules/PricingRulesStrategyFactory";

const productsCache: Record<string, Product> = {}

@injectable()
export class CheckoutService {
  constructor (
    @inject(PricingRulesStrategyFactory) private pricingRulesStrategyFactory: PricingRulesStrategyFactory,
    @inject(CustomerRepository) private customerRepository: CustomerRepository,
    @inject(ProductRepository) private productRepository: ProductRepository
  ) {
    //
  }

  public calculateCheckoutTotal (customerId: string, items: Array<string>): number {
    const customer = this.customerRepository.getCustomer(customerId);

    if (!items.length) {
      throw new HttpError (404, "items required"); 
    }

    // Not rquired
    // if (!customer) {
    //   throw new HttpError (404, "customer not found");
    // }

    const { pricingRules } = customer ?? {};
    const products: Array<Product> = items.map((item) => {
      if (productsCache[item])
        return (productsCache[item]);
      const product = this.productRepository.getProduct(item);
      productsCache[product.id] = product;
      return product;
    });

    // make a copy and do pricing rules in priority order
    const [pricingRuleTotal, remainingProducts] = this.calculatePricingRule([...pricingRules ?? []].sort((a, b) => b.priority - a.priority), products);

    return remainingProducts.reduce((acc, curr) => {
      return acc + curr.price
    }, pricingRuleTotal);
  }
  
  private calculatePricingRule = (pricingRules: Array<PricingRule>, products: Array<Product>, total: number = 0): [number, Array<Product>] => {
    if (pricingRules?.length) {
      const [pricingRule] = pricingRules;

      if (pricingRule.params.productId) {
        const productId = pricingRule.params.productId as string;
        if (productsCache[productId]) {
          pricingRule.params.product = (productsCache[productId]);
        } else {
          const product = this.productRepository.getProduct(productId);
          pricingRule.params.product = product;
        }
      }
     
      const pricingRuleStrategy = 
        this.pricingRulesStrategyFactory.matchPricingRuleStrategy(pricingRule.type, pricingRule.params);
        
      if ((pricingRuleStrategy?.assert(products))) {
        const [pricingRuleTotal, remainingProducts] = pricingRuleStrategy.calculate(products); 
        // same rule
        return this.calculatePricingRule(pricingRules, remainingProducts, pricingRuleTotal + total);
      } else {
        // next rule
        return this.calculatePricingRule(pricingRules.slice(1), products, total);
      }
    }

    return [total, products];
  }
}