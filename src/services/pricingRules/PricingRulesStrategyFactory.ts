import { injectable } from "tsyringe";
import { BasePricingRuleStrategy } from "services/pricingRules/BasePricingRuleStrategy";
import { DiscountPricingRuleStrategy } from "services/pricingRules/DiscountPricingRuleStrategy";
import { XforYPricingStrategy } from "services/pricingRules/XforYPricingStrategy";

@injectable()
export class PricingRulesStrategyFactory {
  // find first instance
  public matchPricingRuleStrategy = (name: string, params: Record<string, any>): BasePricingRuleStrategy | undefined => {
    switch (name) {
      case DiscountPricingRuleStrategy.NAME: return new DiscountPricingRuleStrategy(params);
      case XforYPricingStrategy.NAME: return new XforYPricingStrategy(params);
      default: return undefined;
    }
  }    
}