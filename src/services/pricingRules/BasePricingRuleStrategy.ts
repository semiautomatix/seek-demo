import { Product } from "models/Product";

export abstract class BasePricingRuleStrategy {  
  protected params: Record<string, any>;

  constructor(params: Record<string, any>) {
    this.params = params ?? {};
  }
  public abstract assert: (products: Array<Product>, params?: Record<string, any>) => Boolean;
  public abstract calculate: (products: Array<Product>) => [number, Array<Product>] // total of pricing rule and unused items
}