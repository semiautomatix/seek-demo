import { Product } from "models/Product";
import { BasePricingRuleStrategy } from "services/pricingRules/BasePricingRuleStrategy";

export class DiscountPricingRuleStrategy extends BasePricingRuleStrategy {
  public static NAME = "discount";

  constructor(params: Record<string, any>) {
    super(params);
  }

  // this is the discount rule and we have at least one product remaining that matches discount product
  public assert = (products: Array<Product>) => {
    return this.params.productId && this.params.price && 
      products.filter((product) => product.id === this.params.productId).length > 0
  }

  public calculate = (products: Array<Product>): [number, Array<Product>] => {
    const total = products.reduce((acc, curr) => {
      if (curr.id === this.params.productId) {
        return acc + this.params.price
      }
      return acc;
    }, 0);
    const remainingProducts = products.filter((product) => product.id !== this.params.productId)
    return [total, remainingProducts];
  }
}