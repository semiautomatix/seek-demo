import { Product } from "models/Product";
import { BasePricingRuleStrategy } from "services/pricingRules/BasePricingRuleStrategy";

export class XforYPricingStrategy extends BasePricingRuleStrategy {
  public static NAME = "xfory";

  constructor(params: Record<string, any>) {
    super(params);
  }

  // this is the discount rule and we have at least one product remaining that matches discount product
  public assert = (products: Array<Product>) => {
    return this.params.y && this.params.x && this.params.productId && this.params.product &&
      products.filter((product) => product.id === this.params.productId).length >= this.params.x // must have x products to get it for y's pricing
  }

  public calculate = (products: Array<Product>): [number, Array<Product>] => {
    let count = 0;

    const remainingProducts = products.filter((product) => {
      if (count < this.params.x &&
        product.id === this.params.productId) {
        count++;
        return false;
      }
      return true;
    });

    return [this.params.product.price * this.params.y, remainingProducts];
  }
}