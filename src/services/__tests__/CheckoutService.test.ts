
import "reflect-metadata";
import { container } from 'tsyringe';
import { CustomerRepository } from "repository/CustomerRepository";
import { ProductRepository } from "repository/ProductRepository";
import { CheckoutService } from "services/CheckoutService";
import { mockCustomerRepository } from "mocks/mockCustomerRepository";
import { mockProductRepository } from "mocks/mockProductRepository";

describe('Happy paths', () => {
  let sut: CheckoutService;

  beforeEach(() => {
    // mock out repository
    container.register<CustomerRepository>(CustomerRepository, { useValue: mockCustomerRepository as any })
    container.register<ProductRepository>(ProductRepository, { useValue: mockProductRepository as any })
    sut = container.resolve(CheckoutService);
  })

  const products = {
    "classic": {
      "id": "classic",
      "description": "Classic Ad",
      "price": 269.99
    },
    "standout": {
      "id": "standout",
      "description": "Stand out Ad",
      "price": 322.99
    },
    "premium": {
      "id": "premium",
      "description": "Premium Ad",
      "price": 394.99
    }
  }

  // Customer: default
  // Items: `classic`, `standout`, `premium`
  // Total: $987.97
  test("default", () => {
    // arrange
    const customerId = 'customer-1';
    const items = ["classic", "standout", "premium"];

    mockProductRepository.getProduct.mockReturnValueOnce(products["classic"])
      .mockReturnValueOnce(products["standout"])
      .mockReturnValueOnce(products["premium"]);

    mockCustomerRepository.getCustomer.mockReturnValueOnce(undefined);

    // act
    const result = sut.calculateCheckoutTotal(customerId, items);

    // assert
    expect(result).toEqual(987.97);
  });

  // Customer: SecondBite
  // Items: `classic`, `classic`, `classic`, `premium`
  // Total: $934.97
  test("SecondBite", () => {
    // arrange
    const customerId = 'customer-1';
    const items = ["classic", "classic", "classic", "premium"];
    const customer = {
      id: customerId,
      name: 'customer-2',
      pricingRules: [
        {
          priority: 1,
          type: "xfory",
          params: {
            productId: "classic",
            x: 3,
            y: 2
          }
        }
      ]
    }

    mockProductRepository.getProduct.mockReturnValueOnce(products["classic"])
      // .mockReturnValueOnce(products["classic"])
      // .mockReturnValueOnce(products["classic"]) // should be cached
      .mockReturnValueOnce(products["premium"]);

    mockCustomerRepository.getCustomer.mockReturnValueOnce(customer);

    // act
    const result = sut.calculateCheckoutTotal(customerId, items);

    // assert
    expect(result).toEqual(934.97);
  });

  // Customer: Axil Coffee Roasters
  // Items: `standout`, `standout`, `standout`, `premium`
  // Total: $1294.96
  test("Axil Coffee Roasters", () => {
    // arrange
    const customerId = 'customer-1';
    const items = ["standout", "standout", "standout", "premium"];
    const customer = {
      id: customerId,
      name: 'customer-2',
      pricingRules: [
        {
          priority: 1,
          type: "discount",
          params: {
            productId: "standout",
            price: 299.99
          }
        }
      ]
    }

    mockProductRepository.getProduct.mockReturnValueOnce(products["standout"])
      // .mockReturnValueOnce(products["standout"])
      // .mockReturnValueOnce(products["standout"]) // should be cached
      .mockReturnValueOnce(products["premium"]);

    mockCustomerRepository.getCustomer.mockReturnValueOnce(customer);

    // act
    const result = sut.calculateCheckoutTotal(customerId, items);

    // assert
    expect(result).toEqual(1294.96);
  });
})
