import { Product } from "models/Product";
import { ProductRepository } from "repository/ProductRepository";
import { injectable, inject } from "tsyringe";

@injectable()
export class ProductService {
  constructor (@inject(ProductRepository) private productRepository: ProductRepository) {
    //
  }

  public getProducts = (): Array<Product> => {
    const products = this.productRepository.getProducts();
    return products;
  }
}