export enum PricingRuleType {
  XFORY = 'xfory',
  DISCOUNT = 'discount'
}

export interface PricingRule {
  priority: number,
  type: PricingRuleType,
  params: Record<string, unknown> 
}