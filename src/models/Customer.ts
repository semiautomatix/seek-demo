import { PricingRule } from "models/PricingRule";

export interface Customer {
  id: string,
  name: string,
  pricingRules: Array<PricingRule>
}