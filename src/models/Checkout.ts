import { Customer } from "models/Customer";
import { Product } from "models/Product";

export interface Checkout {
  customerId: Customer['id'],
  items: Array<Product['id']>
}