import { APIGatewayAuthorizerResult, PolicyDocument, Statement, CustomAuthorizerEvent } from "aws-lambda";
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';
import { v4 as uuidv4 } from 'uuid';

export const handler = async (event: CustomAuthorizerEvent): Promise<APIGatewayAuthorizerResult> => {
    winston.add(new WinstonCloudWatch({
        logGroupName: 'authorizer',
        logStreamName: event.requestContext?.requestId ?? uuidv4()
    }));

    const token = event.authorizationToken;

    // base64 admin:password
    // alternatively plainCreds = (new Buffer(encodedCreds, 'base64')).toString().split(':')
    if (token == 'Basic YWRtaW46cGFzc3dvcmQ=') {
        winston.info('Allow', { userId: token });
        return generatePolicy('user', 'Allow', event.methodArn);
    }
    else {
        winston.info('Deny', { userId: token });
        return generatePolicy('user', 'Deny', event.methodArn);
    }
}

// Helper function to generate an IAM policy
const generatePolicy = (principalId: string, effect: string, resource: string): APIGatewayAuthorizerResult => {
    const authResponseContext = {
        userId: 1
    }

    const authResponse: Partial<APIGatewayAuthorizerResult> = {
        context: authResponseContext
    };

    authResponse.principalId = principalId;
    if (effect && resource) {
        const statement: Statement = {
            Action: 'execute-api:Invoke',
            Effect: effect,
            Resource: resource
        }
        const policyDocument: PolicyDocument = {
            Version: '2012-10-17',
            Statement: [statement]
        };
        authResponse.policyDocument = policyDocument;
    }

    winston.info('Response', { authResponse });
    return authResponse as APIGatewayAuthorizerResult;
}


