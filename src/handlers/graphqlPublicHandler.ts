/* handler.js */
import "reflect-metadata"
import { APIGatewayEvent, Context, APIGatewayProxyResult } from "aws-lambda";
import { graphql } from "graphql";
import PublicSchema from "schemas/PublicSchema";
import { container, inject, injectable } from "tsyringe";
import { HttpError } from "handlers/errors/HttpError";

@injectable()
export default class HttpHandler {
  constructor(@inject(PublicSchema) private schema: PublicSchema) {
    //
  }

  public handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    const body = JSON.parse(event.body as string);

    // TODO: middleware to check for body and query
    try {
      const { data, errors } = await graphql({
        schema: this.schema.exectuableSchema(),
        source: body.query
      });

      if (errors) {
        // TODO: expand error handler
        console.error("GraphQL query error", { errors })
        return {
          statusCode: 500,
          body: JSON.stringify({ errors })
        }
      }

      return {
        statusCode: 200,
        body: JSON.stringify(data),
      };
    } catch (ex) {
      console.error("An error occured", { error: ex })
      // TODO: error middleware
      return {
        statusCode: (ex as HttpError).statusCode ?? 500,
        body: JSON.stringify({
          error: (ex as Error).message
        }),
      };
    }
  }
}

const httpHandler = container.resolve(HttpHandler);
export const handler = httpHandler.handler;

