/* handler.js */
import "reflect-metadata"

import { APIGatewayEvent, Context, APIGatewayProxyResult } from "aws-lambda";
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';
import { graphql } from "graphql";
import { container, inject, injectable } from "tsyringe";
import { CustomerSchema } from "schemas/CustomerSchema";
import { HttpError } from "handlers/errors/HttpError";
import { v4 as uuidv4 } from 'uuid';

@injectable()
export default class HttpHandler {
  constructor(@inject(CustomerSchema) private schema: CustomerSchema) {
    //
  }

  public handler = async (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => {
    winston.add(new WinstonCloudWatch({
      logGroupName: 'graphqlCustomerHandler',
      logStreamName: event.requestContext?.requestId ?? uuidv4()
    }));

    const body = JSON.parse(event.body as string);

    // TODO: this shouldevent.headers be extracted from the token
    const customerId = event.headers['Customerid'];

    // TODO: middleware to check for body and query
    try {
      // TODO: this should already be checked by the gateway
      if (!customerId) {
        throw new HttpError (400, "customerId is required");
      }

      const { data, errors } = await graphql({
        schema: this.schema.exectuableSchema(customerId),
        source: body.query
      });

      if (errors) {
        // TODO: expand error handler
        console.error("GraphQL query error", { errors })
        return {
          statusCode: 500,
          body: JSON.stringify({ errors })
        }
      }

      return {
        statusCode: 200,
        body: JSON.stringify(data),
      };
    } catch (ex) {
      console.error("An error occured", { error: ex })
      // TODO: error middleware
      return {
        statusCode: (ex as HttpError).statusCode ?? 500,
        body: JSON.stringify({
          error: (ex as Error).message
        }),
      };
    }
  }
}

const httpHandler = container.resolve(HttpHandler);
export const handler = httpHandler.handler;

