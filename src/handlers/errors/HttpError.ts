export class HttpError extends Error {
  public statusCode: number = 500;

  constructor(statusCode: number, message: string) {
    super(message);
    this.name = 'HttpError';
    this.statusCode = statusCode;
    this.stack = (new Error()).stack;
  }
}