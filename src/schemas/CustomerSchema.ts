import { makeExecutableSchema } from '@graphql-tools/schema';
import { Product } from 'models/Product';
import { CheckoutService } from 'services/CheckoutService';
import { inject, injectable } from 'tsyringe';

const typeDefs = /* GraphQL */ `
  type Query {
    calcTotal(items: [String]!): Float
  }
`;

interface CalcTotalParams {
  items: Array<string>
}

// convert to classes
@injectable()
export class CustomerSchema {
  constructor(
    @inject(CheckoutService) private checkoutService: CheckoutService,
  ) {
    //
  }

  public exectuableSchema(customerId: string) {
    const resolvers = {
      Query: {
        calcTotal: (_: any, { items }: CalcTotalParams): number => this.checkoutService.calculateCheckoutTotal(customerId, items)
      },
    }
    return makeExecutableSchema({
      typeDefs,
      resolvers
    })
  }
}

