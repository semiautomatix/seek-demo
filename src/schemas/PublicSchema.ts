import { makeExecutableSchema } from '@graphql-tools/schema';
import { Product } from 'models/Product';
import { ProductService } from 'services/ProductService';
import { inject, injectable } from 'tsyringe';

const typeDefs = /* GraphQL */ `
  type Product {
    id: String!,
    description: String!,
    price: Float!
  }

  type Query {
    products: [Product]
  }
`;

// convert to classes
@injectable()
class Schema {
  constructor(
    @inject(ProductService) private productService: ProductService,
  ) {
    //
  }

  public exectuableSchema() {
    const resolvers = {
      Query: {
        products: (): Array<Product> => this.productService.getProducts(),
      },
    }
    return makeExecutableSchema({
      typeDefs,
      resolvers
    })
  }
}

export default Schema;
