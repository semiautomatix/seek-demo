import { injectable } from 'tsyringe'
import customers from 'database/customers.json'
import products from 'database/products.json'

@injectable()
export class Database {
  private getAll(tableName: string): Array<any> {
    return ((tableName) => {
      switch(tableName) {
        case 'customers': return customers;
        case 'products': return products;
        default: throw Error('invalid tableName');
      }
    })(tableName)
  }

  private get(tableName: string, id: string) {
    const records = this.getAll(tableName);
    return records.find((record) => record.id === id)
  }

  public get client () {
    return {
      get: this.get,
      getAll: this.getAll
    }
  }

  // TODO: inserts and updates
}