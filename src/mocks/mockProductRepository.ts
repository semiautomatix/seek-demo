export const mockProductRepository = {
  getProduct: jest.fn(),
  getAllProducts: jest.fn()
}