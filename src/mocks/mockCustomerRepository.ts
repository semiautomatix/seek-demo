export const mockCustomerRepository = {
  getCustomer: jest.fn(),
  getAllCustomers: jest.fn()
}