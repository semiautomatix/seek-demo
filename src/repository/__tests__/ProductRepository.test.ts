
import "reflect-metadata";
import { Database } from "database";
import { container } from 'tsyringe';
import { ProductRepository } from "repository/ProductRepository";
import { mockDatabase } from "mocks/mockDatabase";

describe('Happy paths', () => {
  let sut: ProductRepository;

  beforeEach(() => {
    // mock out database calls
    container.register<Database>(Database, { useValue: mockDatabase as any })
    sut = container.resolve(ProductRepository);
  })

  test("Get a product for id", () => {
    const productId = 'product-1';

    // arrange
    const product =
    {
      "id": productId,
      "description": "Product 1",
      "price": 1
    }

    mockDatabase.client.get.mockReturnValue(product);

    // act
    const result = sut.getProduct(productId);

    // assert
    expect(result).toMatchObject(product);
    expect(mockDatabase.client.get).toHaveBeenCalledWith('products', productId);
  });

  test("Get all products", () => {
    // arrange
    const products = [
      {
        "id": "product-1",
        "description": "Product 1",
        "price": 1
      },
      {
        "id": "product-2",
        "description": "Product 2",
        "price": 2
      },
      {
        "id": "product-3",
        "description": "Product 3",
        "price": 3
      }
    ]

    mockDatabase.client.getAll.mockReturnValue(products);

    // act
    const result = sut.getProducts();

    // assert
    expect(result).toMatchObject(products)
    expect(mockDatabase.client.getAll).toHaveBeenCalledWith('products');
  });
})
