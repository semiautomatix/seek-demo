import { Database } from "database";
import { Product } from "models/Product";
import { inject, injectable } from "tsyringe";

@injectable()
export class ProductRepository {
  private static TABLE_NAME = 'products';

  constructor(@inject(Database) private database: Database) {
    //
  }

  public getProduct = (productId: string): Product => {
    return this.database.client.get(ProductRepository.TABLE_NAME, productId);
  }

  public getProducts = (): Array<Product> => {
    return this.database.client.getAll(ProductRepository.TABLE_NAME);
  }
}