import { Database } from "database";
import { Customer } from "models/Customer";
import { inject, injectable } from "tsyringe";

@injectable()
export class CustomerRepository {
  private static TABLE_NAME = 'customers';

  constructor(@inject(Database) private database: Database) {
    //
  }

  public getCustomer = (customerId: string): Customer => {
    return this.database.client.get(CustomerRepository.TABLE_NAME, customerId);
  }

  public getAllCustomers = (): Array<Customer> => {
    return this.database.client.getAll(CustomerRepository.TABLE_NAME);
  }
}