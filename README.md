# seek-demo

Demo project for Seek. This project mimics an AWS environment

## Getting started

Install dependencies
```
yarn
```

### AWS deployment
<p>

Set you AWS keys
```
export AWS_ACCESS_KEY_ID=AKIA3EPH2GCHEMQ***** 
export AWS_SECRET_ACCESS_KEY=sP/DFdBXQwmq3OPl7n0FxSw7Y+JiGsyjQ5g*****
```

Deploy to AWS
```
yarn deploy --config serverless.dev.ts
```

Deploy locally
```
yarn offline --config serverless.dev.ts
```

## Documentation

### Technologies used

- Inversion of Control, using TSyringe
- API Gateway
    - Authentication Lambda
- AWS Lambdas for logic
- Database
    - Mock database using JSON files
- Serverless Framework for deployment
- Winston for logging

## What is missing?

- CORS
- ANY method
- Using a real database
- Serialization middleware
- Schema validation middleware (ajv, layers)
- Unit tests on all files
- Integration or e2e testing
- Authentication via secure provider, such as Cognito or Auth0
- CI/CD
- Canary deployments of Lambdas, using versioning, to limit down time
- Expand the error handling